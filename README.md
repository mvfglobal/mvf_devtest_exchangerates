# MVF Developer Tests
## ExchangeRates

Thanks for your interest in a developer role at MVF.

We have a simple project which we would like you to take a look at in your own time.

You can spend as much or as little time on it as you wish, but if we have asked you to take a look at this, we would usually expect to receive a response in a 3-4 days.

The project is a Codeigniter app written in PHP. It contains a SQLite database for simplicity. You will need PHP, SQLite and PDO to run it. The app contains a Model and Controller which contain functions to manage our exchange rate data. 

### Challenge

How would you improve our code? Fork our repo and let us see your ideas! 

---
### MVF
Do you want to work with the Smartest Tech and the Sharpest Minds? Apply at: http://www.mvfglobal.com/vacancies